{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module Tests.Clash.Hidden.Polysemy where

import           Prelude
import qualified Clash.Prelude as P
import qualified Test.Tasty as T
import qualified Test.Tasty.QuickCheck as T

aEqualsA :: Int -> Bool
aEqualsA a = a == a

tests :: [T.TestTree]
tests =
  [ -- Your tests here
    T.testProperty "Int: a == a" aEqualsA
  ]
