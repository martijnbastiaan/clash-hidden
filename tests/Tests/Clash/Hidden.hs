module Tests.Clash.Hidden where

import           Prelude
import           Test.Tasty

import qualified Tests.Clash.Hidden.Polysemy

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Clash.Feistel"
  [ testGroup "Polysemy" Tests.Clash.Hidden.Polysemy.tests
  ]
