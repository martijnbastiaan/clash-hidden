# Table of Contents

# Setup
You need GHC >=8.4 and Cabal >=2.4 on your PATH. GHC 8.8 and Cabal 3.0 are recommended. To synthesize the designs, use Clash 1.2.

## General (ghcid)
Open a terminal and run

```bash
cabal new-update
cabal new-install ghcid --installdir=$HOME/bin
ghcid -c cabal new-repl
```

This will continuously compile all the exercises in this project. Make sure `$HOME/bin` is on your path. If all is well, you should see something like `All good (13 modules, at 15:53:11)`.

## VSCode (ghcide)
VSCode can use `ghcide` to get compile warnings and errors right in the IDE.

1. Install `ghcide`:

```bash
git clone https://github.com/digital-asset/ghcide.git
cd ghcide
cabal new-update
cabal new-install --installdir=$HOME/bin
```

Make sure `$HOME/bin` is on your path. You can test `ghcide` by running it in this directory.

2. Install the [ghcide VSCode extension](https://marketplace.visualstudio.com/items?itemName=DigitalAssetHoldingsLLC.ghcide).

3. Open this directory in VSCode.

