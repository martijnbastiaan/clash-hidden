{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}

module Clash.Hidden.Polysemy where

import Prelude

import Data.Functor.Identity (Identity, runIdentity)
import Data.Text (Text)
import qualified Data.Text as Text
import Polysemy
import Polysemy.State (State)
import Polysemy.Fixpoint (Fixpoint, fixpointToFinal)
import qualified Polysemy.State as State
import Numeric.Natural (Natural)

import qualified Clash.Explicit.Prelude as C
import qualified Clash.Sized.Internal.BitVector as C

type WithHidden r a = Sem r a
type NoHidden a = Sem '[] a

type ClockS dom = State (C.Clock dom)
type ResetS dom = State (C.Reset dom)
type EnableS dom = State (C.Enable dom)

type HiddenClock dom r =
  ( Member (ClockS dom) r
  , Member Fixpoint r
  , C.KnownDomain dom )

type HiddenReset dom r =
  ( Member (ResetS dom) r
  , Member Fixpoint r
  , C.KnownDomain dom )

type HiddenEnable dom r =
  ( Member (EnableS dom) r
  , Member Fixpoint r
  , C.KnownDomain dom )

type HiddenClockResetEnable dom r =
  ( Member (ClockS dom) r
  , Member (ResetS dom) r
  , Member (EnableS dom) r
  , Member Fixpoint r
  , C.KnownDomain dom )

-- Tracing
type TraceS = State TraceStore

data Trace = Trace {
    t_traceName :: Text
  , t_trace :: [(Natural, Natural)]
  }

data TraceStore = TraceStore{
    ts_traces :: [Trace]
  -- ^ Traces after being converted to BitVector
  , ts_scope :: Text
  -- ^ Name to be prepended to following traces
  }

emptyTraceStore :: TraceStore
emptyTraceStore = TraceStore{
    ts_traces=[]
  , ts_scope=""
  }

data Traces m a where
  SetTrace :: Trace -> Traces m ()
  AppendScope :: Text -> Traces m ()

type HiddenTraces r = Member TraceS r

prependTraceScope :: HiddenTraces r => Text -> Sem r a -> Sem r a
prependTraceScope app sa = do
  traceStore0 <- State.get

  let
    oldScope = ts_scope traceStore0
    newScope = if Text.null oldScope then app else oldScope <> "_" <> app

  State.put (traceStore0{ts_scope=newScope})
  a <- sa
  traceStore1 <- State.get
  State.put (traceStore1{ts_scope=oldScope})

  pure a

traceSignal
  :: (C.BitPack a, HiddenTraces r)
  => Text
  -> C.Signal dom a
  -> Sem r (C.Signal dom a)
traceSignal traceName s = do
  -- TODO: Fetch traceScope
  scope <- ts_scope <$> State.get
  State.modify (\ts -> ts{ts_traces=newTrace scope:ts_traces ts}) >> pure s
 where
   pack (C.pack -> bv) = (C.unsafeToNatural bv, C.unsafeMask bv)

   newTrace scope = Trace {
       t_trace=C.sample (pack <$> s)
     , t_traceName=if Text.null scope then traceName else scope <> "_" <> traceName
     }

ignoreTraces :: WithHidden (TraceS : r) a -> Sem r a
ignoreTraces = State.evalLazyState emptyTraceStore

recoverTraces :: WithHidden (TraceS : r) a -> Sem r (TraceStore, a)
recoverTraces = State.runLazyState emptyTraceStore

-- API
preserveState :: Member (State s) r => Sem r a -> Sem r (s, a)
preserveState sa = do
  s <- State.get
  a <- sa
  State.put s
  pure (s, a)

preserveState_ :: Member (State s) r => Sem r a -> Sem r a
preserveState_ = fmap snd . preserveState

-- | Use a specific clock in a component using one
withClock
  :: HiddenClock dom r
  -- ^ Constraint indicating component uses a clock
  => C.Clock dom
  -- ^ Clock to use in component
  -> WithHidden r a
  -- ^ Component with a hidden clock
  -> WithHidden r a
withClock clk sa = preserveState_ (State.put clk >> sa)

-- | Use a specific reset in a component using one
withReset
  :: HiddenReset dom r
  -- ^ Constraint indicating component uses a reset
  => C.Reset dom
  -- ^ Reset to use in component
  -> WithHidden r a
  -- ^ Component with a hidden reset
  -> WithHidden r a
withReset clk sa = preserveState_ (State.put clk >> sa)

-- | Use a specific enable in a component using one
withEnable
  :: HiddenEnable dom r
  -- ^ Constraint indicating component uses an enable
  => C.Enable dom
  -- ^ Enable to use in component
  -> WithHidden r a
  -- ^ Component with a hidden enable
  -> WithHidden r a
withEnable clk sa = preserveState_ (State.put clk >> sa)

-- | Use a specific enable in a component using one
withClockResetEnable
  :: HiddenClockResetEnable dom r
  -- ^ Constraints indicating component uses a clock, reset, and enable
  => C.Clock dom
  -- ^ Clock to use in component
  -> C.Reset dom
  -- ^ Reset to use in component
  -> C.Enable dom
  -- ^ Enable to use in component
  -> WithHidden r a
  -- ^ Component with a hidden clock, reset, and enable
  -> WithHidden r a
withClockResetEnable clk rst ena =
  withClock clk . withReset rst . withEnable ena

attachClock
  :: C.KnownDomain dom
  => C.Clock dom
  -> WithHidden (ClockS dom : r) a
  -> WithHidden r a
attachClock = State.evalLazyState

attachReset
  :: C.KnownDomain dom
  => C.Reset dom
  -> WithHidden (ResetS dom : r) a
  -> WithHidden r a
attachReset = State.evalLazyState

attachEnable
  :: C.KnownDomain dom
  => C.Enable dom
  -> WithHidden (EnableS dom : r) a
  -> WithHidden r a
attachEnable = State.evalLazyState

attachClockResetEnable
  :: C.KnownDomain dom
  => C.Clock dom
  -> C.Reset dom
  -> C.Enable dom
  -> WithHidden (ClockS dom : ResetS dom : EnableS dom : r) a
  -> WithHidden r a
attachClockResetEnable clk rst ena =
  attachEnable ena . attachReset rst . attachClock clk

sampleN :: C.NFDataX a => Int -> NoHidden (C.Signal dom a) -> [a]
sampleN n = C.sampleN n . run

noHidden :: NoHidden a -> a
noHidden = run

-- TESTS
dflipflop
  :: ( HiddenClock dom r
     , C.NFDataX a )
  => C.Signal dom a
  -> Sem r (C.Signal dom a)
dflipflop inp = do
  clk <- State.get
  pure (C.dflipflop clk inp)

register
  :: forall dom a r
   . ( HiddenClockResetEnable dom r
     , HiddenTraces r
     , C.NFDataX a
     , C.BitPack a )
  => a
  -> C.Signal dom a
  -> Sem r (C.Signal dom a)
register resetValue inp = do
  clk <- State.get
  rst <- State.get
  ena <- State.get
  traceSignal "register" (C.register clk rst ena resetValue inp)

-- | Just a test for multiple hidden clocks + partially propagated constraints
register2
  :: ( HiddenClockResetEnable dom1 r
     , HiddenClockResetEnable dom2 r
     , HiddenTraces r
     , C.NFDataX a
     , C.BitPack a )
  => a
  -> C.Signal dom1 a
  -> C.Signal dom2 a
  -> Sem r (C.Signal dom1 a, C.Signal dom2 a)
register2 resetValue inp1 inp2 = do
  out1 <- register resetValue =<< dflipflop inp1
  out2 <- register resetValue =<< dflipflop inp2
  pure (out1, out2)


main :: IO ()
main = do
  C.printX (C.sampleN 10 sig)
  putStrLn (show (map t_traceName (ts_traces traces)))
 where
  (traces, sig) =
      runIdentity
    $ runFinal
    $ fixpointToFinal @Identity
    $ recoverTraces
    $ attachEnable C.enableGen
    $ attachReset C.resetGen
    $ attachClock C.systemClockGen
    -- $ attachClock C.systemClockGen
    -- ^ Adding this commented line will make GHC infer:
    --
    --     Sem [ ClockS C.System
    --         , ClockS C.System
    --         , [..]
    --         ] (C.Signal C.System Char)
    --
    -- ..which makes sense, but isn't very useful. In practice Polysemy will
    -- always pick the first clock and ignore all the others. In other words,
    -- the /first/ 'attachClock' will have effect, while the ones following
    -- are no-ops.
    $ prependTraceScope "a"
    $ prependTraceScope "b"
    $ mdo
       counter <- register @C.System @Int 3 (counter + 1)
       pure counter
